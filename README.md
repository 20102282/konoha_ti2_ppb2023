<img src="/img/readmebox.svg" alt="readmebox">



# Aplikasi Pencatat Hutang

Aplikasi Pencatatan Hutang adalah sebuah aplikasi yang membantu pengguna untuk mencatat dan mengelola hutang yang dimiliki baik sebagai peminjam maupun sebagai pemberi pinjaman. Aplikasi ini sangat berguna bagi mereka yang memiliki banyak hutang dan sulit mengingat detail dan jatuh tempo hutang mereka. Aplikasi pencatatan hutang juga dapat membantu pengguna untuk melacak jumlah uang yang harus mereka bayar dan menerima, mengelola jadwal pembayaran, dan mengingatkan mereka tentang jatuh tempo pembayaran.

## NOTE
Terkadang data tidak tampil ketika dirun di android

## Anggota Kelompok

IF-08-TI2

1. Eri Masudillah
2. Devin Ayu P
3. Yuni Nur F

## Apa yang bekerja?

- login and register
- CRUD (create, read, delete, update)
- log out

## Apa yang tidak bekerja?
- pembayaran 
- riwayat hutang
- pengaturan akun
- pencarian
- sort


## Instalasi

1. Clone repositori ini ke dalam direktori lokal Anda.

## Galeri


<div style="display: grid; grid-template-columns: repeat(2, 1fr); grid-gap: 10px;">
  <!-- Foto 1 (Vertikal) -->
  <div style="display: flex; flex-direction: column;">
    <img src="/img/homepage.png" alt="Foto 1">
  </div>
  
  <!-- Foto 2 (Vertikal) -->
  <div style="display: flex; flex-direction: column;">
    <img src="/img/about us.png" alt="Foto 2">
  </div>
  
  <!-- Foto 3 (Horizontal) -->
  <div style="display: flex; justify-content: space-between;">
    <img src="/img/hutang.png" alt="Foto 3">
  </div>
  
  <!-- Foto 4 (Horizontal) -->
  <div style="display: flex; justify-content: space-between;">
    <img src="/img/piutang.png" alt="Foto 4">
  </div>
  
  <!-- Foto 5 (Horizontal) -->
  <div style="display: flex; justify-content: space-between;">
    <img src="/img/detail_h.png" alt="Foto 5">
  </div>
  
  <!-- Foto 6 (Vertikal) -->
  <div style="display: flex; flex-direction: column;">
    <img src="/img/akun.png" alt="Foto 6">
  </div>
</div>

