import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/piutang.dart';

class PiutangRepository {
  final CollectionReference piutangCollection =
      FirebaseFirestore.instance.collection('piutang');

  Future<List<Piutang>> getPiutangList() async {
    try {
      final querySnapshot = await piutangCollection.get();
      return querySnapshot.docs.map((doc) {
        final data = doc.data()! as Map<String, dynamic>;
        final timestamp = data['tanggalPinjam'] as Timestamp?;
        final tanggalPinjam =
            timestamp != null ? timestamp.toDate() : null; // Handle null case

        return Piutang(
          id: doc.id,
          namaPeminjam: data['namaPeminjam'] ?? '',
          nominal: data['nominal'] ?? '0.0',
          tanggalPinjam: tanggalPinjam,
          deskripsi: data['deskripsi'] ?? '',
        );
      }).toList();
    } catch (e) {
      throw Exception('Failed to fetch piutang list: $e');
    }
  }

  Stream<List<Piutang>> getPiutangStream() {
    return piutangCollection.snapshots().map((querySnapshot) {
      return querySnapshot.docs.map((doc) {
        final data = doc.data()! as Map<String, dynamic>;
        final timestamp = data['tanggalPinjam'] as Timestamp?;
        final tanggalPinjam =
            timestamp != null ? timestamp.toDate() : null; // Handle null case

        return Piutang(
          id: doc.id,
          namaPeminjam: data['namaPeminjam'] ?? '',
          nominal: data['nominal'] ?? '0.0',
          tanggalPinjam: tanggalPinjam,
          deskripsi: data['deskripsi'] ?? '',
        );
      }).toList();
    });
  }

  Future<void> addPiutang(Piutang piutang) async {
    try {
      await piutangCollection.add({
        'namaPeminjam': piutang.namaPeminjam,
        'nominal': piutang.nominal,
        'tanggalPinjam': piutang.tanggalPinjam,
        'deskripsi': piutang.deskripsi,
      });
    } catch (e) {
      throw Exception('Failed to add piutang.');
    }
  }

  Future<void> updatePiutang(Piutang piutang) async {
    try {
      await piutangCollection.doc(piutang.id).update({
        'namaPeminjam': piutang.namaPeminjam,
        'nominal': piutang.nominal,
        'tanggalPinjam': piutang.tanggalPinjam,
        'deskripsi': piutang.deskripsi,
      });
    } catch (e) {
      throw Exception('Failed to update piutang.');
    }
  }

  Future<void> deletePiutang(String id) async {
    try {
      await piutangCollection.doc(id).delete();
    } catch (e) {
      throw Exception('Failed to delete piutang.');
    }
  }

  Future<void> bayarPiutang(String idPiutang, double jumlahPembayaran) async {
    try {
      // Ambil data piutang berdasarkan ID
      final piutangDoc = piutangCollection.doc(idPiutang);
      final piutangData = await piutangDoc.get();
      final data = piutangData.data()! as Map<String, dynamic>;
      final double piutangNominal = data['nominal'];

      // Hitung sisa piutang setelah pembayaran
      final sisaPiutang = piutangNominal - jumlahPembayaran;

      if (sisaPiutang <= 0) {
        // Jika sisa piutang kurang dari atau sama dengan 0, tandai piutang sebagai lunas di Firebase
        await piutangDoc.update({
          'nominal': 0,
          'lunas': true,
        });
      } else {
        // Jika masih ada sisa piutang, kurangi nominal piutang di Firebase
        await piutangDoc.update({
          'nominal': sisaPiutang,
        });
      }
    } catch (e) {
      throw Exception('Failed to make payment: $e');
    }
  }
}
