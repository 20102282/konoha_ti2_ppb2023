import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/hutang.dart';

class HutangRepository {
  final CollectionReference hutangCollection =
      FirebaseFirestore.instance.collection('hutang');

  Future<List<Hutang>> getHutangList() async {
    try {
      final querySnapshot = await hutangCollection.get();
      return querySnapshot.docs.map((doc) {
        final data = doc.data()! as Map<String, dynamic>;
        final timestamp = data['tanggalPinjam'] as Timestamp?;
        final tanggalPinjam = timestamp != null ? timestamp.toDate() : null;

        return Hutang(
          id: doc.id,
          namaPemberiPinjaman: data['namaPemberiPinjaman'] ?? '',
          nominal: data['nominal'] ?? '0.0',
          tanggalPinjam: tanggalPinjam,
          deskripsi: data['deskripsi'] ?? '',
          lunas:
              data['lunas'] ?? false, // Retrieve the lunas field from Firestore
        );
      }).toList();
    } catch (e) {
      throw Exception('Failed to fetch hutang list: $e');
    }
  }

  Stream<List<Hutang>> getHutangStream() {
    return hutangCollection.snapshots().map((querySnapshot) {
      return querySnapshot.docs.map((doc) {
        final data = doc.data()! as Map<String, dynamic>;
        final timestamp = data['tanggalPinjam'] as Timestamp?;
        final tanggalPinjam = timestamp != null ? timestamp.toDate() : null;

        return Hutang(
          id: doc.id,
          namaPemberiPinjaman: data['namaPemberiPinjaman'] ?? '',
          nominal: data['nominal'] ?? '0.0',
          tanggalPinjam: tanggalPinjam,
          deskripsi: data['deskripsi'] ?? '',
          lunas:
              data['lunas'] ?? false, // Retrieve the lunas field from Firestore
        );
      }).toList();
    });
  }

  Future<void> addHutang(Hutang hutang) async {
    try {
      await hutangCollection.add({
        'namaPemberiPinjaman': hutang.namaPemberiPinjaman,
        'nominal': hutang.nominal,
        'tanggalPinjam': hutang.tanggalPinjam,
        'deskripsi': hutang.deskripsi,
      });
    } catch (e) {
      throw Exception('Failed to add hutang.');
    }
  }

  Future<void> updateHutang(Hutang hutang) async {
    try {
      final double newNominal = hutang.nominal;

      await hutangCollection.doc(hutang.id).update({
        'namaPemberiPinjaman': hutang.namaPemberiPinjaman,
        'nominal': newNominal,
        'tanggalPinjam': hutang.tanggalPinjam,
        'deskripsi': hutang.deskripsi,
        'lunas': newNominal == 0 ? true : false,
      });
    } catch (e) {
      throw Exception('Failed to update hutang.');
    }
  }

  Future<void> deleteHutang(String id) async {
    try {
      await hutangCollection.doc(id).delete();
    } catch (e) {
      throw Exception('Failed to delete hutang.');
    }
  }

  Future<void> bayarHutang(String idHutang, double jumlahPembayaran) async {
    try {
      // Ambil data hutang berdasarkan ID
      final hutangDoc = hutangCollection.doc(idHutang);
      final hutangData = await hutangDoc.get();
      final data = hutangData.data()! as Map<String, dynamic>;
      final double hutangNominal = data['nominal'];

      // Hitung sisa hutang setelah pembayaran
      final sisaHutang = hutangNominal - jumlahPembayaran;

      if (sisaHutang <= 0) {
        // Jika sisa hutang kurang dari atau sama dengan 0, tandai hutang sebagai lunas di Firebase
        await hutangDoc.update({
          'nominal': 0,
          'lunas': true,
        });
      } else {
        // Jika masih ada sisa hutang, kurangi nominal hutang di Firebase
        await hutangDoc.update({
          'nominal': sisaHutang,
        });
      }
    } catch (e) {
      throw Exception('Failed to make payment: $e');
    }
  }
}
