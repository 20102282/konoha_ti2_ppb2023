import 'dart:async';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () => Navigator.pushNamed(context, '/login'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              //logo
              'images/logo.png',
              width: 150, // Sesuaikan lebar gambar
              height: 150, // Sesuaikan tinggi gambar
            ),
            SizedBox(height: 20),
            Text(
              "Catat Hutang Piutang",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xff3D4DE0),
              ),
            ),
            SizedBox(
                height:
                    65), // Menambahkan jarak antara teks di atas dan teks di bawah
            Text(
              "Konoha Team",
              style: TextStyle(
                fontSize: 14, // Ukuran teks lebih kecil
                color: Colors.grey, // Warna abu-abu
              ),
            ),
          ],
        ),
      ),
    );
  }
}
