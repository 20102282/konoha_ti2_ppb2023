import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import '../../repositories/auth_repo.dart';
part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitial());
  final _repo = AuthRepo();
  void register(
      {required String name,
      required String email,
      required String password}) async {
    emit(RegisterLoading());
    try {
      await _repo.register(name: name, email: email, password: password);
      emit(RegisterSuccess('Register success'));
    } catch (e) {
      emit(RegisterFailure(e.toString()));
    }
  }

  void registerFailure(String s) {}

  void registerSuccess(String s) {}
}
