import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../models/piutang.dart';
import '../../../repositories/piutang_repo.dart';

part 'piutang_state.dart';

class PiutangCubit extends Cubit<PiutangState> {
  final PiutangRepository piutangRepository = PiutangRepository();

  PiutangCubit() : super(PiutangInitial());

  Stream<List<Piutang>> getPiutangStream() {
    try {
      final piutangStream = piutangRepository.getPiutangStream();
      return piutangStream.handleError((error) {
        emit(PiutangError('Failed to fetch piutang stream: $error'));
      });
    } catch (e) {
      emit(PiutangError('Failed to fetch piutang stream: $e'));
      return Stream
          .empty(); // Mengembalikan stream kosong jika terjadi kesalahan
    }
  }

  Future<void> getPiutangList() async {
    try {
      emit(PiutangLoading());
      final piutangList = await piutangRepository.getPiutangList();
      emit(PiutangLoaded(piutangList));
    } catch (e) {
      emit(PiutangError('Failed to fetch piutang list.$e'));
    }
  }

  Future<void> addPiutang(Piutang piutang) async {
    try {
      emit(PiutangLoading());
      await piutangRepository.addPiutang(piutang);
      emit(PiutangInitial());
    } catch (e) {
      emit(PiutangError('Failed to add piutang.'));
    }
  }

  Future<void> updatePiutang(Piutang piutang) async {
    try {
      emit(PiutangLoading());
      await piutangRepository.updatePiutang(piutang);
      emit(PiutangInitial());
    } catch (e) {
      emit(PiutangError('Failed to update piutang.'));
    }
  }

  Future<void> deletePiutang(String id) async {
    try {
      emit(PiutangLoading());
      await piutangRepository.deletePiutang(id);
      emit(PiutangInitial());
    } catch (e) {
      emit(PiutangError('Failed to delete piutang.'));
    }
  }

  Future<void> bayarPiutang(String idPiutang, double jumlahPembayaran) async {
    try {
      emit(PiutangLoading());
      await piutangRepository.bayarPiutang(idPiutang, jumlahPembayaran);
      final updatedPiutangList = await piutangRepository.getPiutangList();
      emit(PiutangLoaded(updatedPiutangList));
    } catch (e) {
      emit(PiutangError('Failed to make payment: $e'));
    }
  }
}
