part of 'piutang_cubit.dart';

@immutable
abstract class PiutangState {}

class PiutangInitial extends PiutangState {}

class PiutangLoading extends PiutangState {}

class PiutangLoaded extends PiutangState {
  final List<Piutang> piutangStream;

  PiutangLoaded(this.piutangStream);
}

class PiutangError extends PiutangState {
  final String message;

  PiutangError(this.message);
}
