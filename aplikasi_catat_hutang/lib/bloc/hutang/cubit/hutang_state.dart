part of 'hutang_cubit.dart';

@immutable
abstract class HutangState {}

class HutangInitial extends HutangState {}

class HutangLoading extends HutangState {}

class HutangLoaded extends HutangState {
  final List<Hutang> hutangStream;

  HutangLoaded(this.hutangStream);
}

class HutangError extends HutangState {
  final String message;

  HutangError(this.message);
}
