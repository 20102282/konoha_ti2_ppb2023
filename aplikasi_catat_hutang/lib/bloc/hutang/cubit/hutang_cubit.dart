import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../models/hutang.dart';
import '../../../repositories/hutang_repo.dart';

part 'hutang_state.dart';

class HutangCubit extends Cubit<HutangState> {
  final HutangRepository hutangRepository = HutangRepository();

  HutangCubit() : super(HutangInitial());

  Stream<List<Hutang>> getHutangStream() {
    try {
      final hutangStream = hutangRepository.getHutangStream();
      return hutangStream.handleError((error) {
        emit(HutangError('Failed to fetch hutang stream: $error'));
      });
    } catch (e) {
      emit(HutangError('Failed to fetch hutang stream: $e'));
      return Stream
          .empty(); // Mengembalikan stream kosong jika terjadi kesalahan
    }
  }

  Future<void> getHutangList() async {
    try {
      emit(HutangLoading());
      final hutangList = await hutangRepository.getHutangList();
      emit(HutangLoaded(hutangList));
    } catch (e) {
      emit(HutangError('Failed to fetch hutang list.$e'));
    }
  }

  Future<void> addHutang(Hutang hutang) async {
    try {
      emit(HutangLoading());
      await hutangRepository.addHutang(hutang);
      emit(HutangInitial());
    } catch (e) {
      emit(HutangError('Failed to add hutang.'));
    }
  }

  Future<void> updateHutang(Hutang hutang) async {
    try {
      emit(HutangLoading());
      await hutangRepository.updateHutang(hutang);
      emit(HutangInitial());
    } catch (e) {
      emit(HutangError('Failed to update hutang.'));
    }
  }

  Future<void> deleteHutang(String id) async {
    try {
      emit(HutangLoading());
      await hutangRepository.deleteHutang(id);
      emit(HutangInitial());
    } catch (e) {
      emit(HutangError('Failed to delete hutang.'));
    }
  }

  Future<void> bayarHutang(String idHutang, double jumlahPembayaran) async {
    try {
      emit(HutangLoading());
      await hutangRepository.bayarHutang(idHutang, jumlahPembayaran);
      final updatedHutangList = await hutangRepository.getHutangList();
      emit(HutangLoaded(updatedHutangList));
    } catch (e) {
      emit(HutangError('Failed to make payment: $e'));
    }
  }
}
