import 'package:aplikasi_catat_hutang/views/hutang/hutang_detail.dart';
import 'package:aplikasi_catat_hutang/views/hutang/input_hutang.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/hutang/cubit/hutang_cubit.dart';
import '../../models/hutang.dart';

class HutangItemCard extends StatelessWidget {
  final Hutang hutang;

  const HutangItemCard({required this.hutang});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
          bottom: 16.0), // Tambahkan padding ke bawah card
      child: SizedBox(
        height: 170.0,
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor:
                hutang.lunas ? Colors.green : Color.fromARGB(255, 30, 98, 224),
          ),
          onPressed: () {
            // Tampilkan detail hutang
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HutangDetailPage(hutang: hutang),
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.7,
                      child: Text(
                        hutang.namaPemberiPinjaman,
                        maxLines: 1,
                        style: const TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        // Handle popup menu button actions
                      },
                      child: PopupMenuButton<String>(
                        itemBuilder: (BuildContext context) => [
                          const PopupMenuItem<String>(
                            value: 'edit',
                            child: Text('Edit'),
                          ),
                          const PopupMenuItem<String>(
                            value: 'delete',
                            child: Text('Hapus'),
                          ),
                        ],
                        onSelected: (value) {
                          if (value == 'edit') {
                            // Tampilkan halaman edit hutang
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    InputHutang(hutang: hutang),
                              ),
                            );
                          } else if (value == 'delete') {
                            // Tampilkan konfirmasi hapus hutang
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text('Konfirmasi Hapus'),
                                  content: Text(
                                    'Apakah Anda yakin ingin menghapus hutang ini?',
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Batal'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                        context
                                            .read<HutangCubit>()
                                            .deleteHutang(hutang.id);

                                        // Add message
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          SnackBar(
                                            content:
                                                Text('Hutang berhasil dihapus'),
                                          ),
                                        );
                                      },
                                      child: Text('Hapus'),
                                    ),
                                  ],
                                );
                              },
                            );
                          }
                        },
                        child: Icon(Icons.more_vert_outlined),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Text(
                  hutang.lunas ? 'Lunas' : hutang.nominal.toStringAsFixed(3),
                  textAlign: TextAlign.justify,
                  maxLines: 5,
                  style: const TextStyle(fontSize: 17.0),
                ),
                if (hutang.lunas)
                  Text(
                    'Lunas',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.green,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
