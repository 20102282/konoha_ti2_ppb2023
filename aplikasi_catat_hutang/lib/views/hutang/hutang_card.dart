import 'package:aplikasi_catat_hutang/views/hutang/hutang_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/hutang/cubit/hutang_cubit.dart';
import '../../models/hutang.dart';

class HutangCard extends StatelessWidget {
  final List<Hutang> hutangList;

  const HutangCard({required this.hutangList});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ListView.builder(
        itemCount: hutangList.length,
        itemBuilder: (context, index) {
          final hutang = hutangList[index];
          return HutangItemCard(hutang: hutang);
        },
      ),
    );
  }
}
