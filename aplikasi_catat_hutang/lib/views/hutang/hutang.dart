import 'package:aplikasi_catat_hutang/views/hutang/input_hutang.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/hutang/cubit/hutang_cubit.dart';
import '../../models/hutang.dart';
import '../piutang/sort_options_bottom_sheet.dart';
import 'hutang_card.dart';

class HutangPage extends StatelessWidget {
  const HutangPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        toolbarHeight: 80,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Hutang',
              style: TextStyle(
                fontSize: 24,
                color: Colors.lightBlue,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: 30,
              margin: EdgeInsets.symmetric(horizontal: 16),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        hintText: 'Cari hutang...',
                        hintStyle: TextStyle(fontSize: 16),
                        contentPadding: EdgeInsets.symmetric(horizontal: 16),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return SortOptionsBottomSheet();
                        },
                      );
                    },
                    icon: Icon(Icons.sort),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: BlocProvider<HutangCubit>(
        create: (_) => HutangCubit(),
        child: StreamBuilder<List<Hutang>>(
          stream: context.read<HutangCubit>().getHutangStream(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else if (snapshot.hasData) {
              final hutangList = snapshot.data!;
              return HutangCard(hutangList: snapshot.data!);
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else {
              return Container(); // Modify this container with your desired layout and widgets
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => InputHutang()),
          );
        },
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
