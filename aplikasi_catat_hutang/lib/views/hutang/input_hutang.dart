import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

import '../../bloc/hutang/cubit/hutang_cubit.dart';
import '../../models/hutang.dart';

class InputHutang extends StatefulWidget {
  final Hutang? hutang;

  InputHutang({this.hutang});

  @override
  _InputHutangState createState() => _InputHutangState();
}

class _InputHutangState extends State<InputHutang> {
  final TextEditingController namaPemberiPinjamanController =
      TextEditingController();
  final TextEditingController nominalController = TextEditingController();
  final TextEditingController deskripsiController = TextEditingController();
  late DateTime selectedDate;

  @override
  void initState() {
    super.initState();
    selectedDate = widget.hutang?.tanggalPinjam ?? DateTime.now();
    if (widget.hutang != null) {
      final Hutang hutang = widget.hutang!;
      namaPemberiPinjamanController.text = hutang.namaPemberiPinjaman;
      nominalController.text =
          NumberFormat.currency(locale: 'id', symbol: 'Rp ', decimalDigits: 0)
              .format(hutang.nominal);
      deskripsiController.text = hutang.deskripsi;
    }
  }

  @override
  void dispose() {
    nominalController.dispose();
    super.dispose();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final hutangCubit = context.read<HutangCubit>();

    return Scaffold(
      appBar: AppBar(
        title: widget.hutang != null
            ? Text('Perbarui Hutang')
            : Text('Input Hutang'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Nama Pemberi Pinjaman',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            TextFormField(
              controller: namaPemberiPinjamanController,
              decoration: InputDecoration(
                hintText: 'Masukkan nama pemberi pinjaman',
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Nominal',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            TextFormField(
              controller: nominalController,
              onChanged: (value) {
                if (value.isNotEmpty && !value.startsWith('Rp')) {
                  value = 'Rp $value';
                }
                final formattedValue = NumberFormat.currency(
                  locale: 'id',
                  symbol: '',
                  decimalDigits: 0,
                );

                final parsedValue =
                    num.tryParse(value.replaceAll(RegExp(r'[^0-9]'), ''));

                if (parsedValue != null) {
                  final formattedText = formattedValue.format(parsedValue);
                  final cursorPosition =
                      value.endsWith('Rp') ? 3 : formattedText.length;

                  nominalController.value = TextEditingValue(
                    text: formattedText,
                    selection: TextSelection.fromPosition(
                      TextPosition(offset: cursorPosition),
                    ),
                  );
                }
              },
              decoration: InputDecoration(
                hintText: 'Masukkan nominal hutang',
              ),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 16),
            GestureDetector(
              onTap: () => _selectDate(context),
              child: AbsorbPointer(
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Pilih tanggal peminjaman',
                    suffixIcon: Icon(Icons.calendar_today),
                  ),
                  initialValue: DateFormat('yyyy-MM-dd').format(selectedDate),
                ),
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Deskripsi',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            Expanded(
              child: TextFormField(
                controller: deskripsiController,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  hintText: 'Masukkan deskripsi hutang',
                ),
              ),
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                String namaPemberiPinjaman = namaPemberiPinjamanController.text;
                double nominal = double.tryParse(nominalController.text) ?? 0.0;
                String deskripsi = deskripsiController.text;

                if (namaPemberiPinjaman.isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content:
                            Text('Nama pemberi pinjaman tidak boleh kosong')),
                  );
                } else if (nominal <= 0) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Nominal harus berupa angka')),
                  );
                } else {
                  if (widget.hutang != null) {
                    // Update data hutang yang sudah ada
                    final updatedHutang = Hutang(
                      id: widget.hutang!.id,
                      namaPemberiPinjaman: namaPemberiPinjaman,
                      nominal: nominal,
                      tanggalPinjam: selectedDate,
                      deskripsi: deskripsi,
                    );
                    hutangCubit.updateHutang(updatedHutang);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('Hutang berhasil diperbarui')),
                    );
                  } else {
                    // Tambah data hutang baru
                    final newHutang = Hutang(
                      namaPemberiPinjaman: namaPemberiPinjaman,
                      nominal: nominal,
                      tanggalPinjam: selectedDate,
                      deskripsi: deskripsi,
                      id: '',
                    );
                    hutangCubit.addHutang(newHutang);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('Hutang berhasil ditambahkan')),
                    );
                  }
                  hutangCubit.getHutangStream();
                  Navigator.pop(context);
                }
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  widget.hutang != null ? 'Simpan Perubahan' : 'Simpan',
                  style: TextStyle(fontSize: 18),
                ),
              ),
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
