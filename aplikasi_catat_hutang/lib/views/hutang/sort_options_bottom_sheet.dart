import 'package:flutter/material.dart';

class SortOptionsBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(16),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              'Urutkan Berdasarkan',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.access_time),
            title: Text('Terbaru'),
            onTap: () {
              // Aksi yang ingin dilakukan saat pilihan "Terbaru" dipilih
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.access_time),
            title: Text('Terlama'),
            onTap: () {
              // Aksi yang ingin dilakukan saat pilihan "Terlama" dipilih
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Nama Peminjam'),
            onTap: () {
              // Aksi yang ingin dilakukan saat pilihan "Nama Peminjam" dipilih
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.attach_money),
            title: Text('Nominal'),
            onTap: () {
              // Aksi yang ingin dilakukan saat pilihan "Nominal" dipilih
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.category),
            title: Text('Lainnya'),
            onTap: () {
              // Aksi yang ingin dilakukan saat pilihan "Lainnya" dipilih
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
