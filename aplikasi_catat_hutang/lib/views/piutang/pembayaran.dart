import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:aplikasi_catat_hutang/models/piutang.dart';

import '../../bloc/piutang/piutang_cubit.dart';

class PembayaranPage extends StatefulWidget {
  final Piutang piutang;

  const PembayaranPage({Key? key, required this.piutang}) : super(key: key);

  @override
  _PembayaranPageState createState() => _PembayaranPageState();
}

class _PembayaranPageState extends State<PembayaranPage> {
  final _currencyFormat =
      NumberFormat.currency(locale: 'id', symbol: 'Rp', decimalDigits: 0);

  double jumlahPembayaran = 0.0;
  late String caraBayar;
  late DateTime tanggalPembayaran;
  late String deskripsi;

  @override
  void initState() {
    super.initState();
    tanggalPembayaran = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    final piutangCubit = context.read<PiutangCubit>();

    return Scaffold(
      appBar: AppBar(
        title: Text('Pembayaran'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              onChanged: (value) {
                setState(() {
                  jumlahPembayaran = double.tryParse(value) ?? 0.0;
                });
              },
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Jumlah Pembayaran',
                prefixText: 'IDR ',
              ),
            ),
            SizedBox(height: 16),
            Row(
              children: [
                Text('Tanggal Pembayaran:'),
                SizedBox(width: 8),
                Text(DateFormat('d MMMM yyyy').format(tanggalPembayaran)),
                IconButton(
                  onPressed: () async {
                    final selectedDate = await showDatePicker(
                      context: context,
                      initialDate: tanggalPembayaran,
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2101),
                    );
                    if (selectedDate != null &&
                        selectedDate != tanggalPembayaran) {
                      setState(() {
                        tanggalPembayaran = selectedDate;
                      });
                    }
                  },
                  icon: Icon(Icons.calendar_today),
                ),
              ],
            ),
            SizedBox(height: 16),
            TextField(
              onChanged: (value) {
                caraBayar = value;
              },
              decoration: InputDecoration(labelText: 'Cara Bayar'),
            ),
            SizedBox(height: 16),
            TextField(
              onChanged: (value) {
                deskripsi = value;
              },
              decoration: InputDecoration(labelText: 'Deskripsi'),
            ),
            SizedBox(height: 32),
            ElevatedButton(
              onPressed: () {
                piutangCubit.bayarPiutang(widget.piutang.id, jumlahPembayaran);
                Navigator.pop(context);
              },
              child: Text('Simpan Pembayaran'),
            ),
          ],
        ),
      ),
    );
  }
}
