import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import '../../bloc/piutang/piutang_cubit.dart';
import '../../models/piutang.dart';

class InputPiutang extends StatefulWidget {
  final Piutang? piutang;

  InputPiutang({this.piutang});

  @override
  _InputPiutangState createState() => _InputPiutangState();
}

class _InputPiutangState extends State<InputPiutang> {
  final TextEditingController namaPemberiPinjamanController =
      TextEditingController();
  final TextEditingController nominalController = TextEditingController();
  final TextEditingController deskripsiController = TextEditingController();
  late DateTime selectedDate;

  @override
  void initState() {
    super.initState();
    selectedDate = widget.piutang?.tanggalPinjam ?? DateTime.now();
    if (widget.piutang != null) {
      final Piutang piutang = widget.piutang!;
      namaPemberiPinjamanController.text = piutang.namaPeminjam;
      nominalController.text = piutang.nominal.toStringAsFixed(2);
      deskripsiController.text = piutang.deskripsi;
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final piutangCubit = context.read<PiutangCubit>();

    return Scaffold(
      appBar: AppBar(
        title: widget.piutang != null
            ? Text('Perbarui Piutang')
            : Text('Input Piutang'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Nama Pemberi Pinjaman',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            TextFormField(
              controller: namaPemberiPinjamanController,
              decoration: InputDecoration(
                hintText: 'Masukkan nama pemberi pinjaman',
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Nominal',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            TextFormField(
              controller: nominalController,
              decoration: InputDecoration(
                hintText: 'Masukkan nominal piutang',
              ),
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 16),
            GestureDetector(
              onTap: () => _selectDate(context),
              child: AbsorbPointer(
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Pilih tanggal peminjaman',
                    suffixIcon: Icon(Icons.calendar_today),
                  ),
                  initialValue: DateFormat('yyyy-MM-dd').format(selectedDate),
                ),
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Deskripsi',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 8),
            Expanded(
              child: TextFormField(
                controller: deskripsiController,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  hintText: 'Masukkan deskripsi piutang',
                ),
              ),
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                String namaPemberiPinjaman = namaPemberiPinjamanController.text;
                double nominal = double.tryParse(nominalController.text) ?? 0.0;
                String deskripsi = deskripsiController.text;

                if (namaPemberiPinjaman.isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content:
                            Text('Nama pemberi pinjaman tidak boleh kosong')),
                  );
                } else if (nominal <= 0) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Nominal harus lebih dari 0')),
                  );
                } else {
                  if (widget.piutang != null) {
                    final updatedPiutang = Piutang(
                      id: widget.piutang!.id,
                      namaPeminjam: namaPemberiPinjaman,
                      nominal: nominal,
                      tanggalPinjam: selectedDate,
                      deskripsi: deskripsi,
                    );
                    piutangCubit.updatePiutang(updatedPiutang);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('Piutang berhasil diperbarui')),
                    );
                  } else {
                    final newPiutang = Piutang(
                      namaPeminjam: namaPemberiPinjaman,
                      nominal: nominal,
                      tanggalPinjam: selectedDate,
                      deskripsi: deskripsi,
                      id: '',
                    );
                    piutangCubit.addPiutang(newPiutang);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('Piutang berhasil ditambahkan')),
                    );
                  }
                  piutangCubit.getPiutangStream();
                  Navigator.pop(context);
                }
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  widget.piutang != null ? 'Simpan Perubahan' : 'Simpan',
                  style: TextStyle(fontSize: 18),
                ),
              ),
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
