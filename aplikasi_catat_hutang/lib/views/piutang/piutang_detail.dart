import 'package:flutter/material.dart';
import 'package:aplikasi_catat_hutang/models/piutang.dart';
import 'package:aplikasi_catat_hutang/views/piutang/pembayaran.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class PiutangDetailPage extends StatelessWidget {
  final Piutang piutang;

  const PiutangDetailPage({Key? key, required this.piutang}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Piutang'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Piutang dari',
                  style: TextStyle(fontSize: 16, color: Colors.grey),
                ),
                Text(
                  NumberFormat.currency(
                          locale: 'id', symbol: 'Rp', decimalDigits: 0)
                      .format(piutang.nominal),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(height: 8),
            Text(
              '${piutang.namaPeminjam}',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8),
            Text(
              piutang.tanggalPinjam != null
                  ? DateFormat('d MMMM yyyy').format(piutang.tanggalPinjam!)
                  : 'Tanggal tidak tersedia',
              style: TextStyle(fontSize: 16, color: Colors.grey),
            ),
            SizedBox(height: 16),
            Text(
              'Deskripsi',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            Text(
              piutang.deskripsi,
              style: TextStyle(fontSize: 16, color: Colors.grey),
            ),
            SizedBox(height: 32),
          ],
        ),
      ),
      floatingActionButton: piutang.lunas
          ? null
          : FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PembayaranPage(piutang: piutang)),
                );
              },
              child: Container(
                width: 56.0,
                height: 56.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.blue,
                ),
                child: Center(
                  child: Icon(
                    FontAwesomeIcons.moneyBill,
                    color: Colors.white,
                    size: 20.0,
                  ),
                ),
              ),
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
