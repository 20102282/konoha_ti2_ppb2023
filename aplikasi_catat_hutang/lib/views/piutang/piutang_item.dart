import 'package:aplikasi_catat_hutang/views/piutang/piutang_detail.dart';
import 'package:aplikasi_catat_hutang/views/piutang/input_piutang.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/piutang/piutang_cubit.dart';
import '../../models/piutang.dart';

class PiutangItemCard extends StatelessWidget {
  final Piutang piutang;

  const PiutangItemCard({required this.piutang});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 170.0,
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Color.fromARGB(255, 30, 98, 224),
        ),
        onPressed: () {
          // Tampilkan detail piutang
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PiutangDetailPage(piutang: piutang),
            ),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: Text(
                      piutang.namaPeminjam,
                      maxLines: 1,
                      style: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      // Handle popup menu button actions
                    },
                    child: PopupMenuButton<String>(
                      itemBuilder: (BuildContext context) => [
                        const PopupMenuItem<String>(
                          value: 'edit',
                          child: Text('Edit'),
                        ),
                        const PopupMenuItem<String>(
                          value: 'delete',
                          child: Text('Hapus'),
                        ),
                      ],
                      onSelected: (value) {
                        if (value == 'edit') {
                          // Tampilkan halaman edit piutang
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  InputPiutang(piutang: piutang),
                            ),
                          );
                        } else if (value == 'delete') {
                          // Tampilkan konfirmasi hapus piutang
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('Konfirmasi Hapus'),
                                content: Text(
                                  'Apakah Anda yakin ingin menghapus piutang ini?',
                                ),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text('Batal'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                      context
                                          .read<PiutangCubit>()
                                          .deletePiutang(piutang.id);

                                      // Add message
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                          content:
                                              Text('Piutang berhasil dihapus'),
                                        ),
                                      );
                                    },
                                    child: Text('Hapus'),
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      },
                      child: Icon(Icons.more_vert_outlined),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
              Text(
                piutang.nominal.toStringAsFixed(2),
                textAlign: TextAlign.justify,
                maxLines: 5,
                style: const TextStyle(fontSize: 17.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
