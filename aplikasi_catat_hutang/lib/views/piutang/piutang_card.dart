import 'package:aplikasi_catat_hutang/views/piutang/piutang_item.dart';
import 'package:flutter/material.dart';
import '../../models/piutang.dart';

class PiutangCard extends StatelessWidget {
  final List<Piutang> piutangList;

  const PiutangCard({required this.piutangList});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ListView.builder(
        itemCount: piutangList.length,
        itemBuilder: (context, index) {
          final piutang = piutangList[index];
          return PiutangItemCard(piutang: piutang);
        },
      ),
    );
  }
}
