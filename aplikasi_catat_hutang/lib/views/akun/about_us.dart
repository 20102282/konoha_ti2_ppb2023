import 'package:flutter/material.dart';

class TentangKamiPage extends StatelessWidget {
  const TentangKamiPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Kami'),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Tentang Aplikasi Catat Hutang',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            Text(
              'Aplikasi Catat Hutang adalah aplikasi sederhana yang memudahkan Anda untuk mencatat hutang dan piutang. '
              'Anda dapat dengan mudah mengelola dan memantau daftar hutang dan piutang Anda, serta melakukan pembayaran '
              'atau penerimaan piutang dengan cepat.',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 20),
            Text(
              'Kontak Kami:',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
              'Email: info@catathutang.com',
              style: TextStyle(fontSize: 18),
            ),
            Text(
              'Telepon: 123-456-789',
              style: TextStyle(fontSize: 18),
            ),
          ],
        ),
      ),
    );
  }
}
