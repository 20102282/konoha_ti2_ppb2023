import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'about_us.dart';

class AkunPage extends StatelessWidget {
  const AkunPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final User? user = FirebaseAuth.instance.currentUser;

    void _handleSignOut() async {
      try {
        await FirebaseAuth.instance.signOut();
        // Navigasi kembali ke halaman login
        Navigator.pushReplacementNamed(context, '/login');
      } catch (e) {
        print('Error while signing out: $e');
        // Tampilkan pesan error atau lakukan tindakan lain sesuai kebutuhan
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Akun'),
        backgroundColor: Colors.blue, // Ganti warna latar belakang app bar
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 50,
                backgroundImage: Image.asset(
                  'images/images.jpeg',
                  width: 160, // Set the desired width
                  height: 160, // Set the desired height
                ).image,
              ),
              SizedBox(height: 5),
              Text(
                user?.displayName ?? 'Nama Pengguna',
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text(
                user?.email ?? 'Email Pengguna',
                style: TextStyle(fontSize: 18, color: Colors.grey),
              ),
              SizedBox(height: 40),
              ListTile(
                leading: Icon(Icons.info, color: Colors.blue),
                title: Text(
                  'Tentang Kami',
                  style: TextStyle(fontSize: 18),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TentangKamiPage(),
                    ),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.person, color: Colors.blue),
                title: Text(
                  'Profil Saya',
                  style: TextStyle(fontSize: 18),
                ),
                onTap: () {
                  // TODO: Handle "Profil Saya" option
                },
              ),
              ListTile(
                leading: Icon(Icons.settings, color: Colors.blue),
                title: Text(
                  'Pengaturan',
                  style: TextStyle(fontSize: 18),
                ),
                onTap: () {
                  // TODO: Handle "Pengaturan" option
                },
              ),
              ListTile(
                leading: Icon(Icons.logout, color: Colors.red),
                title: Text(
                  'Keluar',
                  style: TextStyle(fontSize: 18, color: Colors.red),
                ),
                onTap: _handleSignOut,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
