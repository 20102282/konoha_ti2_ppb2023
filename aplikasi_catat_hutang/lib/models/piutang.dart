//piutang model
class Piutang {
  final String id;
  final String namaPeminjam;
  final double nominal;
  final DateTime? tanggalPinjam;
  final String deskripsi;
  final bool lunas; // New property

  Piutang({
    required this.id,
    required this.namaPeminjam,
    required this.nominal,
    required this.tanggalPinjam,
    required this.deskripsi,
    this.lunas = false,
  });

  factory Piutang.fromMap(Map<String, dynamic> map) {
    return Piutang(
      id: map['id'],
      namaPeminjam: map['namaPeminjam'],
      nominal: map['nominal'],
      tanggalPinjam: map['tanggalPinjam'],
      deskripsi: map['deskripsi'],
      lunas: map['lunas'] ?? false, // Override the default value
    );
  }

  Piutang markAsLunas() {
    return Piutang(
      id: this.id,
      namaPeminjam: this.namaPeminjam,
      nominal: this.nominal,
      tanggalPinjam: this.tanggalPinjam,
      deskripsi: this.deskripsi,
      lunas: true,
    );
  }
}
