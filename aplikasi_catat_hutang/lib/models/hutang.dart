class Hutang {
  final String id;
  final String namaPemberiPinjaman;
  final double nominal;
  final DateTime? tanggalPinjam;
  final String deskripsi;
  final bool lunas; // New property

  Hutang({
    required this.id,
    required this.namaPemberiPinjaman,
    required this.nominal,
    required this.tanggalPinjam,
    required this.deskripsi,
    this.lunas = false,
  });

  factory Hutang.fromMap(Map<String, dynamic> map) {
    return Hutang(
      id: map['id'],
      namaPemberiPinjaman: map['namaPemberiPinjaman'],
      nominal: map['nominal'],
      tanggalPinjam: map['tanggalPinjam'],
      deskripsi: map['deskripsi'],
      lunas: map['lunas'] ?? false, // Override the default value
    );
  }

  Hutang markAsLunas() {
    return Hutang(
      id: this.id,
      namaPemberiPinjaman: this.namaPemberiPinjaman,
      nominal: this.nominal,
      tanggalPinjam: this.tanggalPinjam,
      deskripsi: this.deskripsi,
      lunas: true,
    );
  }
}
