import 'package:aplikasi_catat_hutang/ui/login.dart';
import 'package:aplikasi_catat_hutang/ui/splash.dart';
import 'package:aplikasi_catat_hutang/utils/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:aplikasi_catat_hutang/views/home_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/hutang/cubit/hutang_cubit.dart';
import 'bloc/login/login_cubit.dart';
import 'bloc/piutang/piutang_cubit.dart';
import 'bloc/register/register_cubit.dart';
import 'firebase_options.dart';

void main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
  } on Exception catch (e) {
    print(e);
  }
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => LoginCubit()),
        BlocProvider(create: (context) => RegisterCubit()),
        BlocProvider(create: (context) => HutangCubit()),
        BlocProvider(create: (context) => PiutangCubit())
      ],
      child: MaterialApp(
        title: "Aplikasi Catat Hutang",
        debugShowCheckedModeBanner: false,
        navigatorKey: NAV_KEY,
        onGenerateRoute: generateRoute,
        home: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            } else if (snapshot.hasData) {
              return HomeScreen();
            } else if (snapshot.hasError) {
              return const Center(
                child: Text('Something went wrong'),
              );
            } else {
              return const SplashScreen();
            }
          },
        ),
      ),
    );
  }
}
